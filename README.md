# HDC-for-Images

1. To run any model go to CNN or OnlineHD folder and run the main.py file. 
2. Change the configurations from the .env file in the respective folder
3. Visualization is done using notebooks. I used merged data of all models to visualize. If you want to merge multiple CSV files example script is provided.
4. scripts used for training and testing of edge TPU results are in tpu folder


# Modifications after paper review

1. Modifications done after paper review are included in the folder paper_modification
2. To run any model got inside the folder and run the scripts


